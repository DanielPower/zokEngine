function love.load()
    class = require('modules/zokClass'); class.import(require('classes'))
    func = require('modules/zokFunc')
    tiles = require('modules/zokTiles')

    tiles.loadMap('map'); class.addList(tiles, "draw")

    debug = class.instCreate(class.classList.Debug, {x=20, y=30, gridSize=16})
    player = class.instCreate(class.classList.Player, {x=0, y=0})
end

function love.mousepressed(x, y, button, isTouch)
    class.execOnList("mouse", "mouse", x, y, button, isTouch)
end

function love.update(dt)
    class.execOnList("update", "update", dt)
end

function love.draw()
    class.execOnList("draw", "draw")
end
