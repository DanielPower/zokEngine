local self = {}

-- Public Functions
function self.pointDirection(x1, y1, x2, y2) -- Gets the angle between two points
    local diffX = x2 - x1
    local diffY = y2 - y1
    local result = math.atan2(diffY, diffX)

    return(result)
end

function self.pythag(a, b) -- Pythagorean Theorum
    local c = math.sqrt(a^2 + b^2)
    return (c)
end

function self.toGrid(x, r) -- Takes a pixel value and returns its location on the grid
    local newX = (x - (x%r)) / r
    return( newX )
end

function self.isSolid(x, y) -- Checks if there is a solid tile at the given location
    local tile = _zokTiles.checkTileLayer(x, y, 1)
    if tile then
        print(tile.tileID)
    end
end

_zokFunc = self
return(self)
