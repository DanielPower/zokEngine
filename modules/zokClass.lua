local self = {}

self.classList = {}
self.lists = {}

-- Public Functions
function self.import(classes)
    -- Include all objects
    for i=1, #classes do
        local class = require(classes[i])
        self.classList[class.name] = class
    end
end

function self.instCreate(class, args)

    -- Create Class List
    classChain = {class}
    while type(class.parent) == "table" do
        class = class.parent
        table.insert(classChain, class)
    end

    -- Create Instance
    local instance = classChain[#classChain].new(args)
    for i=#classChain-1, 1, -1 do
        local child = classChain[i].new(args)
        self.mergeTables(instance, child)
    end

    -- Instance Creation Message
    local message = {}
    table.insert(message, "Instance created with class chain:")
    for i=1, #classChain do
        table.insert(message, i..": "..classChain[i].name)
    end
    self.print(zokEngine, unpack(message))

    self.addList(instance, classChain[1].name)
    if instance.create then
        instance:create()
    end

    return (instance)
end

function self.addList(inst, list)
    if not self.lists[list] then
        self.lists[list] = {}
    end

    table.insert(self.lists[list], inst)
end

function self.remList(inst, list)
    if self.lists[list] then
        for i=1, #self.lists[list] do
            if self.lists[list][i] == inst then
                table.remove(self.lists[list], i)
                return (true)
            end
        end
    end
    return (false)
end

function self.execOnList(list, func, ...)
    local args = {...}
    local results = {}

    if self.lists[list] then
        for i=1, #self.lists[list] do
            results[i] = self.lists[list][i][func](self.lists[list][i], unpack(args))
        end
        return(results)
    else
        print("Error: List does not exist")
        return (false)
    end
end

function self.print(inst, string, ...)
    local args = {...}
    print(os.date("[%H:%M:%S] ")..--[["["inst.class..":"..inst.id.."] "..]]string)
    for i=1, #args do
        print("----------"..args[i])
    end
end

function self.mergeTables(t1, t2)
    for k, v in pairs(t2) do
        if (type(v) == "table") and (type(t1[k] or false) == "table") then
            merge(t1[k], t2[k])
        else
            t1[k] = v
        end
    end
    return t1
end

_zokClass = self
return (self)
