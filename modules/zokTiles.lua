local self = {}

function self.loadMap(room)
    -- TODO: Use two-dimensional table for ImagesX and imagesY for slightly cleaner code
    self.room = require('assets/rooms/'..room..'/room')
    self.tileSheet =    {}
    self.tileQuad =     {}
    self.tileWidth =    {}
    self.tileHeight =   {}
    self.imageWidth =   {}
    self.imageHeight =  {}
    self.imagesX =      {}
    self.imagesY =      {}

    for i=1, #self.room.tilesets do
        self.tileSheet[i] =     love.graphics.newImage('assets/rooms/'..room..'/'..self.room.tilesets[i].image)
        self.tileQuad[i] =      {}
        self.tileWidth[i] =     self.room.tilesets[i].tilewidth
        self.tileHeight[i] =    self.room.tilesets[i].tileheight
        self.imageWidth[i] =    self.room.tilesets[i].imagewidth
        self.imageHeight[i] =   self.room.tilesets[i].imageheight
        self.imagesX[i] =       self.imageWidth[i] / self.tileWidth[i]
        self.imagesY[i] =       self.imageHeight[i] / self.tileHeight[i]

        for y=0, self.imagesY[i] do
            for x=0, self.imagesX[i] do
                self.tileQuad[i][(y*self.imagesX[i])+x+1] = love.graphics.newQuad(x*self.tileWidth[i], y*self.tileHeight[i], self.tileWidth[i], self.tileHeight[i], self.imageWidth[i], self.imageHeight[i])
            end
        end
    end
end

function self.draw()
    love.graphics.setColor(255, 255, 255, 255)
    for l=1, #self.room.layers do
        for y=0, self.imagesY[1]-1 do
            for x=0, self.imagesX[1]-1 do
                local tile = self.checkTileLayer(x, y, l)
                if tile.tileID then
                    local quad = self.tileQuad[tile.tileSet][tile.tileID]
                    if quad then
                        love.graphics.draw(self.tileSheet[tile.tileSet], quad, x*16, y*16)
                    end
                end
            end
        end
    end
end

function self.pointToTile(x, y)
    newX = _zokFunc.toGrid(x, 32)
    newY = _zokFunc.toGrid(y, 32)
end

function self.checkTile(x, y)
    local result = {}

    for l=1, #self.room.layers do
        result[l] = self.checkTileLayer(x, y, l)
    end

    return (result)
end

function self.checkTileLayer(x, y, l)
    local result = {}
    local tileGID = self.room.layers[l].data[(y*self.room.layers[l].width)+x+1]
    local tileID = nil

    if tileGID then
        for i=1, #self.room.tilesets do
            if self.room.tilesets[i+1] then
                if (tileGID < self.room.tilesets[i+1].firstgid) then
                    tileID = (tileGID - self.room.tilesets[i].firstgid) +1
                    tileSet = i
                    break
                end
            else
                tileID = (tileGID - self.room.tilesets[i].firstgid) +1
                tileSet = i
            end
        end
        result.tileSet = tileSet
        result.tileID = tileID
        result.tileGID = tileGID
        return(result)
    else
        return("invalid tile")
    end
end

_zokTiles = self
return (self)
