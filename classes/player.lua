local Player = {}
Player.name = "Player"
Player.parent = class.classList.Body
function Player.new(args)
    local self = {}
    local tileProperties = nil
    --self.sprite = love.graphics.newImage("assets/player.png")
    self.dir = nil
    self.dirX = nil
    self.dirY = nil
    self.size = 20
    self.speed = 3

    function self:create()
        class.addList(self, "update")
        class.addList(self, "draw")
    end

    function self:update(dt)
        self:inputMovement()
        self:setDirection(self.x, self.y, love.mouse.getX(), love.mouse.getY())
    end

    function self:draw()
        love.graphics.draw(self.sprite, self.x, self.y, self.dir, 1, 1, self.origin.x, self.origin.y)
        love.graphics.rectangle("line", self:getBorder().x1, self:getBorder().y1, self:getBorder().w, self:getBorder().h)
    end

    function self:inputMovement()
        local xMag = 0
        local yMag = 0

        if love.keyboard.isDown('d') then
            xMag = xMag + 1
        end if love.keyboard.isDown('a') then
            xMag = xMag - 1
        end if love.keyboard.isDown('s') then
            yMag = yMag + 1
        end if love.keyboard.isDown('w') then
            yMag = yMag - 1
        end

        mag = func.pythag(xMag, yMag)

        if mag ~= 0 then
            self:moveToPoint(self.x + (xMag/mag)*self.speed, self.y + (yMag/mag)*self.speed)
        end
    end

    return (self)
end

return (Player)
