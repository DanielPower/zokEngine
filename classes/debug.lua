local Debug = {}
Debug.name = "Debug"
function Debug.new(args)
    local self = {}
    local message = "noMessage"
    local x = args.x
    local y = args.y
    local gridSize = args.gridSize


    function self:create()
        class.addList(self, "update")
        class.addList(self, "draw")
        class.addList(self, "mouse")
    end

    function self:mouse(x, y, button, isTouch)
        local tileID = tiles.checkTile(self.mouseXGrid, self.mouseYGrid)
        for tile = 1, #tileID do
            print("Layer "..tile..": "..tileID[tile].tileID)
        end
    end

    function self:update()
        local mouseX = love.mouse.getX()
        local mouseY = love.mouse.getY()
        self.mouseXGrid = func.toGrid(mouseX, gridSize)
        self.mouseYGrid = func.toGrid(mouseY, gridSize)
        self.mouseXView = self.mouseXGrid*gridSize
        self.mouseYView = self.mouseYGrid*gridSize
    end

    function self:draw()
        love.graphics.print(message, x, y)
        love.graphics.rectangle("line", self.mouseXView, self.mouseYView, gridSize, gridSize)
    end

    function self:print(newMessage)
        message = newMessage
    end

    return (self)
end
return (Debug)
