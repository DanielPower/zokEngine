local Body = {}
Body.name = "Body"
function Body.new(args)
    local self = {}
    self.sprite = love.graphics.newImage("assets/rat.png")
    self.x = args.x
    self.y = args.y
    self.origin = args.origin or {['x'] = 0, ['y'] = 0}
    self.border = {0, self.sprite:getWidth(), 0, self.sprite:getHeight()}

    -- Public Functions
    -----------------------------------
    function self:create()
    end

    function self:draw()
        love.graphics.draw(self.sprite, self.x, self.y, zokMath.pointDirection(self.x, self.y, love.mouse.getX(), love.mouse.getY()), 1, 1, self.origin.x, self.origin.y)
    end

    function self:getPos()
        return ({
            ['x'] = self.x,
            ['y'] = self.y
        })
    end

    function self:setDirection(x1, y1, x2, y2)
        self.dir = func.pointDirection(x1, y1, x2, y2)
        self.dirX = math.cos(self.dir)
        self.dirY = math.sin(self.dir)
    end

    function self:moveToPoint(newX, newY)
        self.x = newX
        self.y = newY

        func.isSolid(func.toGrid(self.x, 16), func.toGrid(self.y, 16))
    end

    function self:getBorder(x, y, origin, border)
        local result = {}

        if x and y and origin and border then
            result['x1'] = origin.x + border[1] + x
            result['x2'] = origin.x + border[2] + x
            result['y1'] = origin.y + border[3] + y
            result['y2'] = origin.y + border[4] + y
            result['w'] = x2 - x1
            result['h'] = y2 - y1
        else
            result['x1'] = self.origin.x + self.border[1] + self.x
            result['x2'] = self.origin.x + self.border[2] + self.x
            result['y1'] = self.origin.y + self.border[3] + self.y
            result['y2'] = self.origin.y + self.border[4] + self.y
            result['w'] = result.x2 - result.x1
            result['h'] = result.y2 - result.y1
        end
        return (result)
    end

    return (self)
end
return (Body)
