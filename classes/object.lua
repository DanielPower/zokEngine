local Object = {}
Object.name = "Object"
function Object.new()
    local self = {}

    -- Public Functions
    -----------------------------------
    function self:create()
    end

    function self:update()
    end

    function self:draw()
    end

    return (self)
end
return (Object)
